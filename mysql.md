# mysql

## DDL

```mysql
mysql -u root -p 
create database test1;
show databases;
user dbname
show tables;
drop database dbname;	#删除前做好检查和备份

create table tablename(
col1 type constrains,
col2 type constrains
)

DESC tablename;			#查看表的定义
show create table emp \G;#创建之外还能看表的具体信息（引擎服务跟新时间）

alter
```



**USE 数据库名**
选择要操作的Mysql数据库，使用该命令后所有Mysql命令都只针对该数据库

```mysql
mysql> use RUNOOB;
Database changed
```

**SHOW DATABASES:** 
列出 MySQL 数据库管理系统的数据库列表。

```mysql
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| RUNOOB             |
| cdcol              |
| mysql              |
| onethink           |
| performance_schema |
| phpmyadmin         |
| test               |
| wecenter           |
| wordpress          |
+--------------------+
10 rows in set (0.02 sec)
```

**SHOW COLUMNS FROM \*数据表\*:**
显示数据表的属性，属性类型，主键信息 ，是否为 NULL，默认值等其他信息。

```mysql
mysql> SHOW COLUMNS FROM runoob_tbl;
+-----------------+--------------+------+-----+---------+-------+
| Field           | Type         | Null | Key | Default | Extra |
+-----------------+--------------+------+-----+---------+-------+
| runoob_id       | int(11)      | NO   | PRI | NULL    |       |
| runoob_title    | varchar(255) | YES  |     | NULL    |       |
| runoob_author   | varchar(255) | YES  |     | NULL    |       |
| submission_date | date         | YES  |     | NULL    |       |
+-----------------+--------------+------+-----+---------+-------+
4 rows in set (0.01 sec)
```

- **SHOW INDEX FROM \*数据表\*:**
  显示数据表的详细索引信息，包括PRIMARY KEY（主键）。

  ```mysql
  mysql> SHOW INDEX FROM runoob_tbl;
  +------------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
  | Table      | Non_unique | Key_name | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment |
  +------------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
  | runoob_tbl |          0 | PRIMARY  |            1 | runoob_id   | A         |           2 |     NULL | NULL   |      | BTREE      |         |               |
  +------------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
  1 row in set (0.00 sec)
  ```

- **SHOW TABLE STATUS LIKE [FROM db_name] [LIKE 'pattern'] \G:** 
  该命令将输出Mysql数据库管理系统的性能及统计信息。

  ```mysql
  mysql> SHOW TABLE STATUS  FROM RUNOOB;   # 显示数据库 RUNOOB 中所有表的信息
  
  mysql> SHOW TABLE STATUS from RUNOOB LIKE 'runoob%';     # 表名以runoob开头的表的信息
  mysql> SHOW TABLE STATUS from RUNOOB LIKE 'runoob%'\G;   # 加上 \G，查询结果按列打印
  ```













## SQL优化

```mysql
show status like 'com_%';  #通过该命令了解各种SQL的执行频率

```

通过观察SQL大致比例，

推断数据库时更新操作为主还是查询操作为主；

对于事务型应用，观察提交和回滚操作，回滚频繁可能应用编写有问题；



### 定位执行效率低的sql语句（慢查询日志）

只能在查询后才记录，要在查询的进行期间定位有问题的SQL语句要用其他方法

```mysql
set long_query_time=2   #慢查询时间为2秒
						#set global long_query_time=0.01微秒级
select count(1) from emp t1,dept t2 where t1.id=t2.id;
...(11.31 sec)			#大于2秒所以会被记录到慢查询日志
#查看慢查询日志，可以看到执行时间超过2秒的SQL语句
more localhost-slow.log
.....
select count(1) from emp t1,dept t2 where t1.id=t2.id;  #出现sql语句结果

#利用mysqldumpslow工具进行分类汇总
mysqldumpslow bj37-slow.log
#结果会显示超时的SQL语句   
```

查询进行期间定位问题SQL语句的其他方法

使用show processlist 命令查看在进行的线程，实时查看











































