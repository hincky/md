# Docker 可移植性，轻量级

Docker 是一个开源的应用容器引擎，让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的 Linux 机器上，也可以实现虚拟化。

**容器**是完全使用沙箱机制，相互之间不会有任何接口（类似 iPhone 的 app）,更重要的是容器性能开销极低。

利用 Docker 的方法来快速交付，测试和部署代码，您可以大大减少编写代码和在生产环境中运行代码之间的延迟。

## centos安装

### 卸载旧版本

```shell
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```

### 安装之前要设置仓库

```shell
sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2
```

使用命令稳定仓库

```shell
sudo yum-config-manager \
    --add-repo \
    http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

### 安装 Docker Engine-Community

```
sudo yum install docker-ce docker-ce-cli containerd.io
```

查看docker版本：docker version

启动 Docker。

```shell
sudo systemctl start docker
```

通过运行 hello-world 映像来验证是否正确安装了 Docker Engine-Community 。

```shell
sudo docker run hello-world
```

##  配置镜像加速器

通过修改daemon配置文件/etc/docker/daemon.json来使用加速器

```shell
sudo mkdir -p /etc/docker sudo tee /etc/docker/daemon.json <<-'EOF'

> {  

>"registry-mirrors": ["https://mvl6n9ol.mirror.aliyuncs.com"] 

>} 

>EOF 

sudo systemctl daemon-reload sudo systemctl restart docker
```

### 检查加速器是否生效

```yaml
 docker info    # 看是否有以下信息
# Registry Mirrors:
    # https://reg-mirror.qiniu.com
```

### 配置docker服务

```shell
#避免每次使用docker命令都切换特权身份，将当前用户加入docker用户组
sudo usermod -aG docker USER_NAME
#开启debug模式，监听2376端口
docker -D -H tcp://127.0.0.1:2376  #也可写入/etc/docker的daemon.json 中
{
	"debug":true,
	"hosts":["tcp://127.0.0.1:2376"]
}
#修改后重启docker服务,还要通过docker info 确保服务正常运行
#centos的docker配置文件在/etc/systemd/system/docker.service.d/docker.conf
sudo systemctl daemon-reload
sudo systemctl start docker.service
docker info
```

若服务工作不正常，查看日志，以下是centos

```shell
journalctl -u docker.service
```



> DevOps（开发，运维）

更快速交付和部署，利用docker打包镜像发布测试，一键运行

#### 常用命令

```shell
docker version
docker info
docker 命令 --help

```

官方帮助文档https://docs.docker.com/reference/

#### 镜像命令

```shell
docker images
-a #列出所有镜像
-q #只显示镜像的id
#搜索xxx，且收藏数超过4，关键字为TensorFlow
docker search xxx --filter=stars=4 tensorflow
docker pull xxx #下载
docker tag ubuntu:latest myubuntu:latest #添加标签，多一个myubuntu标签的镜像
docker rmi xxx  #删除镜像
docker inspect xxx #查看xxx镜像的详细信息
docker history xxx #查看xxx镜像历史
docker image prune -f #自动清理临时的遗留镜像，释放空间

docker rmi -f id1 id2 #id为容器id
docker rmi -f $(docker images -a)#查询出所有并删除

```

#### 创建镜像（commit，import，build）

基于已有容器创建

```shell
#先启动一个镜像并进行修改，修改可以是在镜像里面创建文件，修改文件等
docker run -it ubuntu:18.04 /bin/bash
touch test
exit
#xxx为原来容器的id
docker [container] commit -m "Added a new file" -a "Docker Newbee" xxx test:0.1
```



基于本地模板导入

```shell
cat ubuntu-18.04xx.tar.gz | docker import - ubuntu:18.04

docker images
```



基于dockerfile创建

```dockerfile
FROM debian:stretch-slim

LABEL version="1.0" maintainer="docker user <docker_user@github>"

RUN apt-get update && \
	apt-get install -y python3 && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/*
```

```shell
docker [image] build -t python:3
```

存出和载入镜像

```shell
#导出镜像到指定文件中，之后可以通过复制tar文件分享给他人
docker save -o ubuntu_18.04.tar ubunt:18.04

#将tar文件导入到本地镜像库
docker load -i ubuntu_18.04.tar 或 docker load < ubuntu_18.04.tar
```

镜像上传

```shell
#在docker hub注册后可传镜像上去
docker push user/test:latest

```





#### 容器命令create,start,run,wait,logs

```shell
docker ps #当前运行中的容器

docker run [-x] imagename
#参数说明
-it 	#使用交互方式运行，进入容器查看内容
-p		#指定容器端口
	-p 主机端口（即公网能访问的）:容器端口
	-p ip（即主机名）: 主机端口:容器端口
	-p 容器端口
	容器端口
--name Name  #给容器自定义命名
-d		#后台方式运行
#此时想进入容器则执行
docker attach 容器id   或者
docker exec -it 容器id bashShell
	
docker exec 容器id #退出容器但不停止容器
docker cp 容器id:容器内路径 目的主机路径 #拷贝到目的主机


#下面是导出容器快照到本地文件xxx.tar
docker export 容器id > xxx.tar
#使用 docker import 从容器快照文件中再导入为镜像，以下实例将快照文件 ubuntu.tar 导入到镜像 test/ubuntu:v1:
cat docker/ubuntu.tar | docker import - test/ubuntu:v1

#测试，启动并进入容器，注意root旁边的主机名称
docker run -it 镜像名 /bin/bash

exit 或者 ctrl+d #退出容器
ctrl+p+q #容器不停止退出
docker stop 容器id #停止容器

docker rm 容器id #删除指定容器，但不能删除正在运行的
docker rm -f xxx #强制删除
docker ps -a -q | xargs docker rm#查询出所有运行的容器，并逐个删除

docker start xxid
docker restart xxid
docker kill xxid
```

#### 日志命令

![image-20200721232805768](C:\Users\Hincky\AppData\Roaming\Typora\typora-user-images\image-20200721232805768.png)

![image-20200721232900857](C:\Users\Hincky\AppData\Roaming\Typora\typora-user-images\image-20200721232900857.png)

```shell
docker inspect 容器id #查看容器的元数据
```

#### 常用命令总结

![image-20200721233854011](C:\Users\Hincky\AppData\Roaming\Typora\typora-user-images\image-20200721233854011.png)

![image-20200721233924975](C:\Users\Hincky\AppData\Roaming\Typora\typora-user-images\image-20200721233924975.png)

安装Nginx

```shell
docker search nginx #搜索Nginx

```







![image-20200721234839641](C:\Users\Hincky\AppData\Roaming\Typora\typora-user-images\image-20200721234839641.png)





### 容器数据卷

容器之间的数据共享技术，通过**挂载主机目录和容器目录**，两个目录的**数据互相同步**

直接使用命令来过载。 

```shell
docker run -it -v 主机目录:容器目录 镜像名 /bin/bash

```

在主机执行**docker inspect 容器id**查看信息，找“Mounts”

看到两个目录已经关联







### DockerFile

用来构建docker镜像的文件，

1. 编写一个dockerfile文件
2. docker build 构建成一个镜像
3. docker run 运行镜像
4. docker push 发布镜像（DockerHub，阿里云）









运维

查找当前目录一个月（30天）以前大于100M的日志文件（.log）并删除

```shell
find .-name".log"-mtime +30 -type f -size +100M | xargs rm -rf {}
#  . 在当前目录
# name指定文件名
# mtime指定修改时间（以天为单位）+xx修改时间大于xx天 -xx修改时间小于xx天
#xargs把前一命令输入当作后一命令输出，通常配合管道使用

grep -A5 custer f1.txt
# 匹配文本文件f1.txt中的custer，并打印出改行以及下面5行
```



vlan隔离冲突和广播域，避免一个局域网产生广播风暴时全部主机遭殃



```shell
01 07 * * 5 /usr/bash/ /usr/local/run.sh
#对应  分 时 日 月 周
```



服务器状态码

```shell
403 #服务器拒绝请求
301 #请求的网页已永久移动到新的位置
503 #服务器无法使用
```

