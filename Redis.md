# Redis

## 引语

### 数据库，缓存，消息中间件MQ

![image-20200716011217296](C:\Users\Hincky\AppData\Roaming\Typora\typora-user-images\image-20200716011217296.png)

​		redis是在存储系统中使用最广泛的中间件，面试也最常问。掌握基础使用方法，也要深层理解内部实现的细节原理。

​		redis不只可以拿来做数据缓存，简单的get/set方法，还有分布式锁（要清楚内部实现机制，和知道怎么使用）。**redis是单线程结构，支持高并发。基于内存。默认有16个数据库**

**瓶颈是内存和网络带宽**

为什么单线程还这么快：基于内存，所有数据放在内存，所以用单线程效率最高，没有上下文切换

## 基础和应用篇

Remote Dictionary Service（远程字典服务）= redis 

面试中“redis是用来干什么的”：缓存，分布式锁。

本书没有的：redis内置的Lua脚本引擎（很少用）

### Redis可以做什么

1.记录帖子的点赞数、评论数、点击数（hash）

2.记录用户的帖子ID列表（排序），便于快速显示用户的帖子列表（zset）

3.记录帖子的标题、摘要、作者和封面信息，用于列表页展示（hash）

4.记录帖子的点赞用户ID列表，评论ID列表，用于显示和去重计数（zset）

5.缓存近期热帖内容，减少数据库压力（hash）

6.记录帖子的相关文章，根据内容推荐相关帖子（list）

7.如果帖子ID是整数自增的，可以使用Redis来分配帖子ID（计数器）

8.收藏集和帖子之间的关系（zset）

9.记录热帖ID列表，总热榜和分类热榜（zset）

10.缓存用户浏览历史，过滤恶意行为（zset，hash）

### Redis基础数据结构

#### String字符串，list列表，hash字典，set集合，zset有序集合

后四种是容器类型结构：

1.对象不存在就创建，再操作。

2.容器里元素都没了，立即删除容器，释放内存。

所有类型都可以设置过期时间，时间到就自动删除对象。如果用set修改了，过期时间会消失。

#### String

**字符数组**。redis所有的数据结构都以唯一的key字符串作为名称，通过这一key值获取value数据。不同类型在value的结构不同。

存用户信息将其转为JSON格式字符串并缓存进redis，拿的时候反序列化。

redis字符串是**动态字符串**，即可以修改的字符串，内部实现类似java的ArrayList，内部分配的实际空间capacity一般高于实际字符串长度len。<u>每次扩容当前的加倍，最大512MB</u>

```bash
-------------------------------键值对
> set name hincky
ok
>append name "ok" #往字符串后面追加ok
>strlen name      #字符串的长度  8
> get name
"hinckyok"
>getrange name 0 3 #截取字符串[0,3], 0 -1 则是所有字符
"hinc"
>setrange name 1xx #替换指定位置开始的字符
"hxxc"
> exists name 
(integer) 1
> del name        #或者move name
(integer) 1
>get name
(nil)
>type name        #查看当前类型
string
--------------------------------批量键值对
>set name1 hin
>set name2 cky
>mget name1 name2
1)"hin"
2)"cky"
>mset name1 hin name2 cky
--------------------------------过期删除
>set name hincky
>expire name 5   #5秒后被删了，为nil
>ttl name 		 #查看当前key剩余的时间
>setex name 5 hincky
--------------------------------如果不存在即创建
>setnx name hincky
--------------------------------计数
如果value是一个整数，还可以对它进行自增。在signed long的最大最小里面。
>set age 10
>incr age  #decr是减
(integer) 11
>incrby age 5 #同理也有decrby，设置增减的步长
(integer) 16

```

#### list列表（栈，队列）双向链表。

redis的链表，相当于java里面的LinkedList。每个元素都使用双向指针顺序，支持向前向后遍历。

插入和删除很快，O（1）

遍历很慢，O（n）

常用作异步队列，将需要延后处理的任务序列化成字符串塞进redis，另一个线程从该列表中轮询处理

##### 队列（右进左出）常见

```bash
>rpush books python java golang
(integer)3
>llen books
(integer)3
>lpop books
"python"
>lpop books
"java"
>lpop books
"golang"
>lpop books
(nil)
```

##### 栈（右进右出）少见

```bash
# 那list作栈的情况比较少
>rpush books java python golang
(integer)3
>rpop books
"golang"
```

##### 慢操作lindex，<u>性能慢O(n)</u>

lindex相当于java链表的get(index)，遍历性能随着index增大而变低

**ltrim**<u>性能慢O(n)</u>,在两个参数start_index和end_index定义了一个区间，保留区间，区间外不要。-1表示倒数第一个，同理-2

```bash
>rpush books java python golang
(integer)3
>lindex books 1    #O（n），慎用，因为性能不好，降低系统效率
"java"
>lrange books 0 -1 #获取所有元素，O(n)，慎用
1）"java"
2）"python"
3）"golang"
>ltrim books 1 -1  #O（n），慎用
ok
>lrange books 0 -1 
1)"java"
2)"golang"
-----------------------------
>ltrim books 1 0   #清空整个列表，因为区间为负，正数全不要。

```

##### 快速列表

quicklist=多个ziplist，使用双向指针串起来

#### hash

无序，key只能是string。数组+链表，发生hash碰撞时采用渐进式rehash策略。

查新旧两个hash结构，在后续的定时任务以及hash操作，逐渐迁移到新的hash结构中。

可以部分获取信息，不像string要一次全获取。但是hash存储消耗高于string

```bash
>hset books java "think in java"   #也可用于更新，只不过返回0
>hgetall books
1)java
2)"think in java"  #key 和 value间隔出现
>hlen books
(integer) 1
>hset user-hincky age 18
>hincrby user-hinkcy age 1
(integer)19
```

#### set（去重，无序）

无序唯一，内部实现相当于一个特殊字典，value都是null。可用于中奖用户，中奖必唯一，不重复。

```bash
>sadd books python 
>sadd books golang
>smembers books
1)"python"
2)"golang"
>sismember books java  #相当于contains（）#返回0
>scard books #相当于count（）
>spop books

```

### zset(唯一，有序)

内部是一个set保证唯一性，同时给每个value赋予一个score，作为排序权重。“跳跃列表”实现

支持随机插入和删除

```bash
>zadd books 9.0 "think in java"
>zadd books 8.9 "java concurrency"
>zadd books 8.6 "java cookbook"
>zrange books 0 -1 #按score排序输出，参数区间为排名范围
1)"java cookbook"
2)"java concurrency"
3)"think in java"
>zrevrange books 0 -1 #按score排序输出，参数区间为排名范围
1)"think in java"
2)"java concurrency"
3)"java cookbook"
>zcard books #3
>zscore books "java concurrency" #获取内部的score
>zrangebyscore books -inf 8.91 withscores #根据分值区间，inf为无穷大
>zrem books "java concurrency"  #删除value

```

跳跃列表，最下面一层所有的元素串起来，几个元素挑一个代表，在几个代表挑一个使用另外指针串起来。最大31层

### 分布式锁

限制程序的并发执行，使得多线程读写过程中不出现错乱。

```bash
>set lock:hincky true ex 5 nx #拿到锁之后加上过期时间，保证set和expire组合成原子操作，避免set和									expire之间出现断电之类，造成死锁。
....do something 
>del lock:hincky
```

**分布式锁不要用于长时间的任务**（避免前一进程过期了，临界区的业务逻辑还没执行完就把锁给了下一进程）



## 原理篇和集群篇



## 拓展篇

stream

![640?wx_fmt=png](https://ss.csdn.net/p?https://mmbiz.qpic.cn/mmbiz_png/8XkvNnTiapOOhqYNVd3YMNhq94CAEpCuibKK08cZrw89qyh0fmcgDw7gR9pwp1CvDPibiaHxuPbnibv7Pg8BK5hhlvw/640?wx_fmt=png)



## 源码篇





## 应用篇

购买了服务器之后，获取服务器ip地址，重置服务器密码，就可远程登录  

进入后台的ECS云服务器管理页面，看到运行的实例

安全组：在linux的防火墙端口开完之后，还需要在阿里云设置安全组规则。即开放端口号，否则外界无法访问

默认安全组规则不用动，只要关内网入方向全部规则（端口：-1/-1,http80/80,https443/443,ssh22/22,ftp21/21）

授权对象（开放白名单）：0.0.0.0/0

使用xshell连接

ctrl+滚轮放大字体



#### 下载

下载压缩包，到自己选定的目录进行解压，进入解压后的文件

5.0版本的redis：

```bash
yum install gcc-c++ 
make
make install
```

6.0版本

```bash
// 查看gcc版本
gcc -v

#升级到 5.3及以上版本
yum -y install centos-release-scl
yum -y install devtoolset-9-gcc devtoolset-9-gcc-c++ devtoolset-9-binutils

scl enable devtoolset-9 bash

#注意：scl命令启用只是临时的，推出xshell或者重启就会恢复到原来的gcc版本。
#如果要长期生效的话，执行如下：

echo "source /opt/rh/devtoolset-9/enable" >>/etc/profile

#回到redis解压后的文件
make
```



但是由于我们的redis是在阿里云上的，为了外网能够访问，必须修改redis配置，
vi redis.conf
修改配置如下:
redis只监听本地的127.0.0.1的6379端口，外网传入的请求是无法接收的，修改bind 127.0.0.1为
bind 0.0.0.0
Redis默认不是以守护进程的方式运行，可以通过该配置项修改，使用yes启用守护进程，设置为no
daemonize no
保护模式,关闭保护模式，否则外部ip无法连接
protected-mode no
打开注释，修改foobare为你的密码
requirepass foobared
重启
redis-server redis.conf



#### 连接redis

先启动redis：redis-server redis.conf  #这里的具体文件的redis配置文件

redis-cli -p 6379

![image-20200716003724850](C:\Users\Hincky\AppData\Roaming\Typora\typora-user-images\image-20200716003724850.png)

连接一个服务服务之后,通过管道查看过滤有redis的信息

ps -ef|grep redis

#### 关闭redis

shutdown关闭redis

exit退出



#### redis-benchmark压力测试工具

在一个终端开启并连接了redis之后，打开另一个终端

执行redis-benchmark -h localhost -p 6379 -c 100 -n 100000

#在本机上，6379端口，测试100个并发连接十万个请求



#### 基本知识

16个数据库，编号0-15，通过select n来选择使用具体数据库

dbsize 来查询当前数据库的数据长度

flushdb清楚当前数据库

flushall清空全部数据库

