## Git

只能跟踪文件的改动，但图片视频等二进制文件，无法跟踪变化。

![image-20200713012611912](C:\Users\Hincky\AppData\Roaming\Typora\typora-user-images\image-20200713012611912.png)

工作目录：包含.git 的文件，如repo/.git  repo就是工作目录。要跟踪的文件必须放在工作目录下。

工作区：就是电脑上看到的目录

版本库：就是隐藏目录.git   里面包含了stage，还有自动创建的第一个分支master的指针HEAD

一般在分支上干活，分支代码稳定后再合并到主分支master







##### 创建版本库（repository）

打开一个盘，然后mkdir  xxx

查看当前目录 **pwd**

将这个目录设置成git可以管理的仓库**git init**    这时候目录有.git的隐藏文件



#### 实例demo

git目录下创建txt文件，写下内容

创建文件:**touch xxx.txt**

添加到暂存区**git add xxx.txt**

提价到仓库 **git commit -m "xxx.txt提交"**       括号内是注释

查看是否还有文件未提交**git status** 

再来修改txt的内容

并用**git status**查看结果 ，提示"Changes not staged for commit"

查看txt文件修改了什么内容，用**git diff xxx.txt**   红色为原来内容，绿色为修改

提交修改，二步：git add+git commit（同上）

##### 查询日志

查看历史记录**git log**   显示的是从最近到最远的日志

如果日志太多，可以用**git log --pretty=oneline**

##### 版本回退

第一种：**git reset --hard HEAD^**   如果是退回到上上个版本，就用两个^,或数字2

利用cat xxx.txt 来查看验证

第二种：在关过命令行，或不知道版本号时，用**git reflog** 将列出所有版本

找到想要恢复的版本号eg：6cfcfc89 指令：**git reset --hard 6cfcfc89**

再用cat 查看内容确认



##### 撤销修改

在未提交之前，发现内容有误，要恢复到之前版本

第一种：知道要删除的内容，手动去掉，**add+commit**

第二种：按照上一个方法恢复到上一版本**git reset --hard HEAD^**

第三种：撤销命令：

撤销前查看状态：git status

提示使用**git checkout -- file** 就可丢弃工作区的修改，一定要有--，不然创建分支

这种撤销有两种情况

a:文件没有放到暂存区，直接恢复到和版本库一模一样的状态

b:进入了暂存区且做了修改，则恢复到修改前进入暂存区的状态



##### 删除文件

删除已提交的文件：**rm xx.txt**

查看**git status**，一是可以提交删除后的版本；二是恢复到被删除之前

一：commit；二：git checkout -- xx.txt



##### 远程仓库

Git和GitHub传输是通过SSH加密的，要设置

1：创建SSh密钥，在用户目录下，看是否有.ssh目录，有就看有没id_rsa和id_rsa.pub这两个文件

如果没有就**ssh-keygen -t  rsa -C "GitHub账号"**

id_rsa是私钥，不可泄露，id_rsa.pub是公钥

2：登录GitHub打开settings中的SSH Keys页面，再“Add SSH Key”，任意title，在key里面粘贴id_rsa.pub内容

在git上**ssh -T git@github.com** 测试一下

添加远程仓库：

在Git有仓库后，想在GitHub创建一个Git仓库，让两个仓库同步，GitHub作为备份

在GitHub“Crete a new repo”，输入仓库名后创建新仓库“create repository”

在本地Git仓库里执行**git remote add  origin https://github.com/xxx/xxx.git**

把本地仓库分支master内容推送到远程仓库**git push -u origin master**

因为远程仓库是空的，用-u把两个仓库的master分支关联起来

现在只要本地提交了修改，就可通过**git push origin master**使两仓库同步



##### 从远程仓库克隆

GitHub已有仓库而本地没的前提下，在本地**git clone https://github.com/xx/xx**



##### 创建分支、合并分支

**git checkout -b dev**创建并切换分支（**git branch dev** + **git checkout dev**）

前者是创建，后者是切换

**git branch**查看当前的分支，显示星号且绿色的就是当前分支

先切换回master分支**git checkout master**,此时并不会出现dev分支的内容

合并分支**git merge dev**,此时可以查看到原本查看不了的dev内容

合并之后可以删除dev分支了**git branch -d dev**



##### 解决冲突

创建新分支，进行文件修改，作了添加和提交后，切换回master

在文件里修改与新分支里文件不一样的内容，添加并提交，形成冲突前提

合并两个分支，产生冲突，<<<HEAD 当前分支修改内容；>>>新分支   新分支的

add+commit 提交master的修改



##### bug分支

当前工作需要两天完成，而bug分支只要五个小时完成，可是手头上的分支还不鞥提价怎么办？

利用stash功能，把当前工作现场隐藏起来，比如在当前的两天分支未提交工作区中使用**git stash**,然后查看状态**git status**，是干净的

然后就可以创建bug分支进行bug修复了，修复完add+commit，切换回主分支合并，删除bug分支即可

再切回到原来手头上的dev分支**git checkout dev**继续两天的工作

利用**git stash list**查看工作现场，然后进行工作现场的恢复

1：git stash apply恢复，恢复后stash内容并不删除，要**git stash drop**删除，一次删一条

2：使用git stash pop ，恢复的同时把stash内容也删除



##### 多人协作

推送：

查看远程库的信息**git remote** 

查看远程库的详细信息 **git remote -v**

将本地更新好并提交的代码推送到远程库中**git push origin master**

若是推送到其他分支，将master名字改为其他分支名即可

抓取：

在其他电脑（要把SSH密钥添加到GitHub上）或同一电脑的其他目录克隆

进入新目录中，克隆到本地中**git clone https://github.com/xxx/xx**

若要在dev分支做开发，必须把远程的origin的dev分支到本地中

利用**git checkout -b dev origin/dev**，将新建的dev和远程的dev关联

修改添加并提交后 ,**git push origin dev**把现在的dev分支推送到远程

可是当不同人进行同一个文件的同一个地方修改，只能推送一个，否则会冲突

这时第一个推送之后的人要先从远程先拉取最新的推送，然后在本地合并，再推送。

通常的细分拉取**git pull <remote> <branch>**



在拉取前要指定本地dev分支与远程origin/dev分支的链接

git branch --set-upstream dev origin/dev

**git pull **拉取，拉取后还是有冲突，要解决，然后在push



因此，可以试图用**git push origin branch-name**推送自己的修改.
如果推送失败，则因为远程分支比你的本地更新早，需要先用**git pull**试图合并。
如果合并有冲突，则需要解决冲突，并在本地提交。再用**git push origin branch-name**推送



### 代码库

#### 一、新建代码库

```html
# 在当前目录新建一个Git代码库
$ git init
 
# 新建一个目录，将其初始化为Git代码库
$ git init [project-name]
 
# 下载一个项目和它的整个代码历史
$ git clone [url]

```

#### 二、配置

```html
# 显示当前的Git配置
$ git config --list
 
# 编辑Git配置文件
$ git config -e [--global]
 
# 设置提交代码时的用户信息
$ git config [--global] user.name "[name]"
$ git config [--global] user.email "[email address]"
```

#### 三、增加/删除文件

```html
# 添加指定文件到暂存区
$ git add [file1] [file2] ...
 
# 添加指定目录到暂存区，包括子目录
$ git add [dir]
 
# 添加当前目录的所有文件到暂存区
$ git add .
 
# 添加每个变化前，都会要求确认
# 对于同一个文件的多处变化，可以实现分次提交
$ git add -p
 
# 删除工作区文件，并且将这次删除放入暂存区
$ git rm [file1] [file2] ...
 
# 停止追踪指定文件，但该文件会保留在工作区
$ git rm --cached [file]
 
# 改名文件，并且将这个改名放入暂存区
$ git mv [file-original] [file-renamed]
```

#### 四、代码提交

```html
# 提交暂存区到仓库区
$ git commit -m [message]
 
# 提交暂存区的指定文件到仓库区
$ git commit [file1] [file2] ... -m [message]
 
# 提交工作区自上次commit之后的变化，直接到仓库区
$ git commit -a
 
# 提交时显示所有diff信息
$ git commit -v
 
# 使用一次新的commit，替代上一次提交
# 如果代码没有任何新变化，则用来改写上一次commit的提交信息
$ git commit --amend -m [message]
 
# 重做上一次commit，并包括指定文件的新变化
$ git commit --amend [file1] [file2] ...
```

#### 五、分支

```html
# 列出所有本地分支
$ git branch
 
# 列出所有远程分支
$ git branch -r
 
# 列出所有本地分支和远程分支
$ git branch -a
 
# 新建一个分支，但依然停留在当前分支
$ git branch [branch-name]
 
# 新建一个分支，并切换到该分支
$ git checkout -b [branch]
 
# 新建一个分支，指向指定commit
$ git branch [branch] [commit]
 
# 新建一个分支，与指定的远程分支建立追踪关系
$ git branch --track [branch] [remote-branch]
 
# 切换到指定分支，并更新工作区
$ git checkout [branch-name]
 
# 切换到上一个分支
$ git checkout -
 
# 建立追踪关系，在现有分支与指定的远程分支之间
$ git branch --set-upstream [branch] [remote-branch]
 
# 合并指定分支到当前分支
$ git merge [branch]
 
# 选择一个commit，合并进当前分支
$ git cherry-pick [commit]
 
# 删除分支
$ git branch -d [branch-name]
 
# 删除远程分支
$ git push origin --delete [branch-name]
$ git branch -dr [remote/branch]
```

#### 六、标签

```html
# 列出所有tag
$ git tag
 
# 新建一个tag在当前commit
$ git tag [tag]
 
# 新建一个tag在指定commit
$ git tag [tag] [commit]
 
# 删除本地tag
$ git tag -d [tag]
 
# 删除远程tag
$ git push origin :refs/tags/[tagName]
 
# 查看tag信息
$ git show [tag]
 
# 提交指定tag
$ git push [remote] [tag]
 
# 提交所有tag
$ git push [remote] --tags
 
# 新建一个分支，指向某个tag
$ git checkout -b [branch] [tag]
```

#### 七、查看信息

```html
# 显示有变更的文件
$ git status
 
# 显示当前分支的版本历史
$ git log
 
# 显示commit历史，以及每次commit发生变更的文件
$ git log --stat
 
# 搜索提交历史，根据关键词
$ git log -S [keyword]
 
# 显示某个commit之后的所有变动，每个commit占据一行
$ git log [tag] HEAD --pretty=format:%s
 
# 显示某个commit之后的所有变动，其"提交说明"必须符合搜索条件
$ git log [tag] HEAD --grep feature
 
# 显示某个文件的版本历史，包括文件改名
$ git log --follow [file]
$ git whatchanged [file]
 
# 显示指定文件相关的每一次diff
$ git log -p [file]
 
# 显示过去5次提交
$ git log -5 --pretty --oneline
 
# 显示所有提交过的用户，按提交次数排序
$ git shortlog -sn
 
# 显示指定文件是什么人在什么时间修改过
$ git blame [file]
 
# 显示暂存区和工作区的差异
$ git diff
 
# 显示暂存区和上一个commit的差异
$ git diff --cached [file]
 
# 显示工作区与当前分支最新commit之间的差异
$ git diff HEAD
 
# 显示两次提交之间的差异
$ git diff [first-branch]...[second-branch]
 
# 显示今天你写了多少行代码
$ git diff --shortstat "@{0 day ago}"
 
# 显示某次提交的元数据和内容变化
$ git show [commit]
 
# 显示某次提交发生变化的文件
$ git show --name-only [commit]
 
# 显示某次提交时，某个文件的内容
$ git show [commit]:[filename]
 
# 显示当前分支的最近几次提交
$ git reflog
```

八、远程同步

```html
# 下载远程仓库的所有变动
$ git fetch [remote]
 
# 显示所有远程仓库
$ git remote -v
 
# 显示某个远程仓库的信息
$ git remote show [remote]
 
# 增加一个新的远程仓库，并命名
$ git remote add [shortname] [url]
 
# 取回远程仓库的变化，并与本地分支合并
$ git pull [remote] [branch]
 
# 上传本地指定分支到远程仓库
$ git push [remote] [branch]
 
# 强行推送当前分支到远程仓库，即使有冲突
$ git push [remote] --force
 
# 推送所有分支到远程仓库
$ git push [remote] --all
```

九、撤销

```html
# 恢复暂存区的指定文件到工作区
$ git checkout [file]
 
# 恢复某个commit的指定文件到暂存区和工作区
$ git checkout [commit] [file]
 
# 恢复暂存区的所有文件到工作区
$ git checkout .
 
# 重置暂存区的指定文件，与上一次commit保持一致，但工作区不变
$ git reset [file]
 
# 重置暂存区与工作区，与上一次commit保持一致
$ git reset --hard
 
# 重置当前分支的指针为指定commit，同时重置暂存区，但工作区不变
$ git reset [commit]
 
# 重置当前分支的HEAD为指定commit，同时重置暂存区和工作区，与指定commit一致
$ git reset --hard [commit]
 
# 重置当前HEAD为指定commit，但保持暂存区和工作区不变
$ git reset --keep [commit]
 
# 新建一个commit，用来撤销指定commit
# 后者的所有变化都将被前者抵消，并且应用到当前分支
$ git revert [commit]
 
# 暂时将未提交的变化移除，稍后再移入
$ git stash
$ git stash pop
```

